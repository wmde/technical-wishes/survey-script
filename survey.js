//script to display a voting tool in wikipages
//insert <div class="votingTool"/> into the wikipage
mw.loader.using( ['oojs','oojs-ui-widgets','oojs-ui.styles.icons-layout'], function () {
    if ( mw.config.get('wgPageName') !== 'Examples/Umfrage/Themen' &&  mw.config.get('wgPageName') == "edit") { return; }

    var options = [];

    var intro = new OO.ui.LabelWidget( {
        align: 'top',
        label: 'Vote your favorite 3 topics in 3 simple steps:'
    } );
    $('.votingTool').append(intro.$element);

    // step 1: select topics
    var step1 = new OO.ui.FieldLayout( new OO.ui.ButtonWidget( {
        label: 'Go to topics',
        href:'#'
    } ), {
        align: 'top',
        label: 'Step 1: Select your topics above',
        help: 'Select via the toggles above. Then click on step 2 and prioritize your selection. ' +
            'If you are happy with the selection and the order, submit your vote.\u200e'
    } );
    $('.votingTool').append(step1.$element);

    // read focus area as options and display toggle button
    $("h3").each(function() {
        const toggle = new OO.ui.ToggleSwitchWidget();
        $( this ).prepend( toggle.$element );
        toggle.setData( $( this ).text() );
        options.push( toggle );
    });

    // step 2: priorization button and input field
    const prioButton = new OO.ui.ButtonInputWidget( {
        label: 'Start priorisation',
        type: 'submit',
        useInputTag: true,
        flags: [
            'progressive'
        ]
    } );
    prioButton.$element.on( 'click', prioHandler );

    var step2 = new OO.ui.FieldLayout( prioButton, {
        align: 'top',
        label: 'Step 2: Prioritize your selection'
    } );
    $('.votingTool').append(step2.$element);

    // sortable input field for priorization
    var priolistWidget = new OO.ui.TagMultiselectWidget( {
        placeholder: "Order your votes by prio via drag & drop (max 3)",
        allowArbitrary: true,
        tagLimit: 3
    } );

    $('.votingTool').append(prioButton.$element);
    $('.votingTool').append(priolistWidget.$element);

    // step 3: submit prioritized values
    const submitButton = new OO.ui.ButtonInputWidget( {
        type: 'submit',
        label: 'Submit your vote',
        flags: [
            'primary',
            'progressive'
        ]
    } )
    submitButton.$element.on('click', submitValues);

    var step3 = new OO.ui.FieldLayout( submitButton, {
        align: 'top',
        label: 'Step 3: Submit your top 3 topics'
    } );
    $('.votingTool').append(step3.$element);


    function prioHandler() {
        var votes = [];
        var list = [];

        options.forEach( function( item ) {
            if( item.getValue() ) {
                votes.push( item.getData().split('|')[0] );
                list.push(item.getData());
            }
        } );

        priolistWidget = new OO.ui.TagMultiselectWidget( {
            placeholder: "Order your votes by prio via drag & drop (max 3)",
            selected: list,
            allowArbitrary: true,
            tagLimit: 3
        } );

        //TODO: change values of widget instead of removing
        $('.oo-ui-tagMultiselectWidget').remove();
        $('.votingTool').append(priolistWidget.$element);
        $('.votingTool').append(submitButton.$element);
    }

    function submitValues() {
        var data = priolistWidget.getValue();
        $('.commentboxInput').val( data.toString() );
        $('.commentbox').find('.cdx-button').click();
    }
} );
