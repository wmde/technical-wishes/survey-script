# survey script

Experimental code that can be used as user script to enhance the survey voting experience.

# dev setup
* checkout branch survey-tool in docker dev
* start docker with module SurveyScript
* store js script in http://dev.wiki.local.wmftest.net:8080/wiki/MediaWiki:Common.js
* add css changes to http://dev.wiki.local.wmftest.net:8080/wiki/MediaWiki:Common.css 
* go to http://dev.wiki.local.wmftest.net:8080/wiki/Examples/Umfrage/Themen